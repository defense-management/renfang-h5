import request from '@/utils/request'

export function login (data) {
  return request({
    url: '/app/az/one',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'application/json;charset=utf-8',
    },
  })
}

export function submit (data) {
  return request({
    url: '/app/az/submit',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'application/json;charset=utf-8',
    },
  })
}
