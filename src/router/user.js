export default [
  {
    path: '/QRCodePage',
    name: 'QRCodePage',
    component: () => import(/* webpackChunkName: "QRCodePage" */ 'views/user/QRCodePage.vue'),
    meta: {
      title: '人防疏散地域系统'
    }
  },
  {
    path: '/InfoPage',
    name: 'InfoPage',
    component: () => import(/* webpackChunkName: "QRCodePage" */ 'views/user/InfoPage.vue'),
    meta: {
      title: '人防疏散地域系统'
    }
  }
]
